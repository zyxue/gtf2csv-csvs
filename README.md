This repo stores csv files converted from gtf using gtf2csv
(https://github.com/zyxue/gtf2csv).





### URLs:

For urls where the original gtf are downloaded, see `urls.txt` under each
subdirectory.

### index gtf

reference: http://www.htslib.org/doc/tabix.html

```
for gtf in $(find . -name '*.gtf.gz'); do
    gtf_prefix=$(dirname ${gtf})/$(basename ${gtf} .gz)
    echo gtf_prefix: ${gtf_prefix}
    unpigz -c ${gtf_prefix}.gz | \grep -v ^"#" | sort -k1,1 -k4,4n | bgzip > ${gtf_prefix}.sorted.gz
    tabix ${gtf_prefix}.sorted.gz
done
```

### sed urls

```
echo sed -e 's/pub\/release-93\/out.txt:/ftp\:\/\/ftp\.ensembl.org/' files_list.txt > urls.txt
```

### Fetch file list

```
\grep -r 'release-.*Homo_sap.*[0-9].gtf.gz' pub/ > files_list.txt
```
