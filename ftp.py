import os
import multiprocessing
from ftplib import FTP, error_perm


def walk_dir(ftp, dirpath, opf):
    """
    adopted from
    https://erlerobotics.gitbooks.io/erle-robotics-python-gitbook-free/file_transfer_protocol_ftp/detecting_directories_and_recursive_download.html
    """
    original_dir = ftp.pwd()
    try:
        ftp.cwd(dirpath)
    except error_perm:
        # for non-directores and ones we cannot enter, asssume it's a file
        print(dirpath)
        opf.write(f'{dirpath}\n')
        return

    names = ftp.nlst()
    for name in names:
        walk_dir(ftp, dirpath + '/' + name, opf)
    ftp.cwd(original_dir)  # return to cwd of our caller


def walk_dir_wrapper(dir_):
    print(f'working on {dir_}')
    outdir = os.path.join('.', dir_.lstrip('/'))
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    out = os.path.join(outdir, 'out.txt')

    ftp = FTP('ftp.ensembl.org')
    ftp.login(user='anonymous', passwd='')
    with open(out, 'wt', buffering=1) as opf:
        walk_dir(ftp, dir_, opf)
    ftp.quit()


if __name__ == "__main__":
    with open('./ftp_list_pub.txt', 'rt') as inf:
        dirs = [_.strip() for _ in inf.readlines()]

    with multiprocessing.Pool(processes=len(dirs)) as p:
        p.map(walk_dir_wrapper, dirs)
